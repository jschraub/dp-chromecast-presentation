define([], function () {
    var ChromecastLibs = {available: true};

    var is_chrome = ((navigator.userAgent.toLowerCase().indexOf('chrome') > -1) && (navigator.vendor.toLowerCase().indexOf("google") > -1));

    if (is_chrome) { // Load the libraries for chrome
        ChromecastLibs = require([
            '//www.gstatic.com/cv/js/sender/v1/cast_sender.js?loadCastFramework=1'
        ], function () {
            return {available: false};
        });
    }

    return ChromecastLibs;
});