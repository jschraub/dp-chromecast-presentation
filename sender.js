define(['./scripts/chromecastLibs'], function () {
    const APP_ID = 'D1D8C6DC',
          MESSAGE_NAMESPACE = 'urn:x-cast:chromecast-slides';
    let castApi,
        chromecastApi;

    const whenCastApiReady = ( successCallback, errorCallback ) => {
        // if we have the cast API available already, just call the successCallback
        if ( window.chrome && window.chrome.cast && window.chrome.cast.isAvailable ) {
            successCallback();
        } else {
            // this is provided by Google for Initialization of Chromecast
            window.__onGCastApiAvailable = ( loaded, errorInfo ) => {
                if ( loaded ) {
                    successCallback();
                } else {
                    errorCallback( errorInfo );
                }
            };
        }
    }

    const initializeCastApi = (castApi, chromecastApi) => {
        var castOptions = new castApi.CastOptions( {
            receiverApplicationId: APP_ID,
            autoJoinPolicy: chromecastApi.AutoJoinPolicy.ORIGIN_SCOPED
        } );
        var castContext = castApi.CastContext.getInstance();
        castContext.setOptions( castOptions );
    }

    const initButtonListeners = () => {
        document.getElementById('button-prev').addEventListener('click', () => { sendMessage('previous') });
        document.getElementById('button-next').addEventListener('click', () => { sendMessage('next')});
        document.getElementById('button-first').addEventListener('click', () => { sendMessage('first')});
        document.getElementById('button-last').addEventListener('click', () => { sendMessage('last')});
        document.getElementById('button-start').addEventListener('click', () => { castApi.CastContext.getInstance().requestSession().then(() => {
                initMessageListeners();
        }, () => {
            var temp = arguments;
        })});
    }

    const initMessageListeners = () => {
        castApi.CastContext.getInstance().getCurrentSession().addMessageListener(MESSAGE_NAMESPACE, (namesapce, message) => {
            let msg = JSON.parse(message);
            document.getElementById('slide-info').innerHTML = msg.slideContent;
        });
    }

    const sendMessage = message => {
        castApi.CastContext.getInstance().getCurrentSession().sendMessage(MESSAGE_NAMESPACE, message);
    }

    const initChromecast = () => {
        whenCastApiReady( () => {
             chromecastApi = window.chrome.cast;
             castApi = window.cast.framework;
            initializeCastApi(castApi, chromecastApi);
            initButtonListeners();
        }, (error) => {
            console.log(error);
        });
    }

    initChromecast();
});